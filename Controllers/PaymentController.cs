﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace Coaching.Controllers
{
    public class PaymentController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var PublicKey = ConfigurationManager.AppSettings["StripePublicKeyClientside"].ToString();

            ViewBag.StripePublicKey = PublicKey;

            return View();
        }

        [HttpPost]
        public ActionResult PostPayment()
        {
            // Set your secret key. Remember to switch to your live secret key in production.
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["StripeSecretKeyServerside"].ToString(); //"sk_test_a7TyTZ0PpsYvFAf5tEW5091T00UCPZigm4";//Secret Key Test -- Need to pull from app config

            try
            {
                var stripetoken = Request.Form["stripeToken"];
                var email = Request.Form["hdnEmailAddress"];

                // Token is created using Checkout or Elements!
                // Get the payment token submitted by the form:


                //Create or retreive a Customer
                var customerOptions = new CustomerCreateOptions
                {
                    Email = email,
                    Description = "Coaching Customer",
                    Source = stripetoken
                };

                var customerService = new CustomerService();
                var customer = customerService.Create(customerOptions);

                if(customer != null)
                {
                    var options = new ChargeCreateOptions
                    {
                        Amount = 6900,
                        Currency = "gbp",
                        Description = "Coaching Fees",
                        Source = customer.DefaultSourceId, //stripetoken,
                        ReceiptEmail = email,
                        Customer = customer.Id
                    };
                    var service = new ChargeService();
                    var charge = service.Create(options);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return RedirectToAction("Index","Thanks");
        }
    }
}