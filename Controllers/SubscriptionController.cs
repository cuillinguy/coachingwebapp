﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Coaching.Controllers
{
    public class SubscriptionController : Controller
    {
     
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                var PublicKey = ConfigurationManager.AppSettings["StripePublicKeyClientside"].ToString();

                ViewBag.StripePublicKey = PublicKey;

                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult PostPayment()
        {
            // Set your secret key. Remember to switch to your live secret key in production.
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["StripeSecretKeyServerside"].ToString();

            try
            {

                var selectedOption = Request.Form["hdnSelectedPrice"];

                if (!string.IsNullOrEmpty(selectedOption))
                {
                    //Check that the amounts have not been tampered with

                    long amount = Convert.ToInt64(selectedOption);


                    var stripetoken = Request.Form["stripeToken"];
                    var email = Request.Form["hdnEmailAddress"];

                    // Token is created using Checkout or Elements!
                    // Get the payment token submitted by the form:


                    //Create or retreive a Customer
                    var customerOptions = new CustomerCreateOptions
                    {
                        Email = email,
                        Description = "Coaching Customer",
                        Source = stripetoken
                    };

                    var customerService = new CustomerService();
                    var customer = customerService.Create(customerOptions);

                    if (customer != null)
                    {
                        var options = new ChargeCreateOptions
                        {
                            Amount = amount, //6900,//Pull through from form
                            Currency = "gbp",
                            Description = "Coaching Fees",
                            Source = customer.DefaultSourceId, //stripetoken,
                            ReceiptEmail = email,
                            Customer = customer.Id
                        };
                        var service = new ChargeService();
                        var charge = service.Create(options);
                    }

                }
                return RedirectToAction("Index", "Thanks");

            }
            catch (Exception ex)
            {

                throw ex;

            }
        }
    }
}