﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stripe;

namespace Coaching.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [HttpGet]
        public  ActionResult Payment()
        {
            return View();
        }
        [HttpPost]
        public ActionResult PostPayment()
        {
            // Set your secret key. Remember to switch to your live secret key in production.
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            StripeConfiguration.ApiKey = "sk_test_a7TyTZ0PpsYvFAf5tEW5091T00UCPZigm4";//Secret Key Test -- Need to pull from app config

            var stripetoken = Request.Form["stripeToken"];


            // Token is created using Checkout or Elements!
            // Get the payment token submitted by the form:
            //var token = "something"; // model.Token; // Using ASP.NET MVC

            var options = new ChargeCreateOptions
            {
                Amount = 395,
                Currency = "gbp",
                Description = "Coaching Fees",
                Source = stripetoken,
            };
            var service = new ChargeService();
            var charge = service.Create(options);

            return RedirectToAction("Thanks");
        }

        public ActionResult Thanks()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}